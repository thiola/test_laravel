<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('foo', function () {
    return 'Hello World';
});
/*
Route::match(['get','post'],'/foo',function(){
  return 'Hello World!!!';
});

Route::any('/foo',function(){
  return 'Hello World!!!';
});

Route::any('/foo',function(){
  return 'Hello World!!!';
});
*/
/*
//localhost:8000/foo
Route::any('/foo',function(){
  return redirect('redirect');
});
*/
//localhost:8000/showRedirect
Route::get('/showRedirect',function(){
  return '<h1>Hi redirect route here</h1>';
})->name('redirect');

//localhost:8000/test
Route::view('/test','test');

//localhost:8000/user/(angka)
Route::get('user/{id}',function($id){
  return '<h1>User '.$id."</h1>";
});

//localhost:8000/post/(angka)/coments/(comment)
Route::get('/post/{post}/coments/{comment}',function($postId,$commentId){
  echo "Post ".$postId;
  echo "<br>";
  echo "Comments ".$commentId;
});

//localhost:8000/user/(nama)
Route::get('user/{name?}',function($name=null){
  return $name;
});

//localhost:8000/user
Route::get('user/{name?}',function($name='Kevin Thiola'){
  return $name;
});


Route::get('/home',function(){
  return '<h1>Your age is less then 100 or equal 100</h1>';
});

Route::group(['middleware'=>'Age'],function(){
  Route::get('/user/age/{age}',function(){
    return '<h1>You have permmision to use this application</h1>';
  });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
